package co.edu.unimagdalena.mycalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    int[] numbersID = new int[]{R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9};
    int[] operationID = new int[]{R.id.btn_sqrt, R.id.btn_div, R.id.btn_minus, R.id.btn_mult, R.id.btn_plus, R.id.btn_equal};
    Button[] numbers = new Button[10];
    Button[] operations = new Button[6];
    Button ac, del, dot, ans;
    TextView output;

    Double current = 0.0;
    Double lastValue = 0.0;
    String lastop = "";

    boolean isNew = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int i = 0; i < numbersID.length; i++) {
            numbers[i] = findViewById(numbersID[i]);
            numbers[i].setOnClickListener(this);
        }

        for (int i = 0; i < operationID.length; i++) {
            operations[i] = findViewById(operationID[i]);
            operations[i].setOnClickListener(this);
        }

        ac = findViewById(R.id.btn_ac);
        ac.setOnClickListener(this);
        del = findViewById(R.id.btn_del);
        del.setOnClickListener(this);
        dot = findViewById(R.id.btn_dot);
        dot.setOnClickListener(this);
        ans = findViewById(R.id.btn_ans);
        ans.setOnClickListener(this);

        output = findViewById(R.id.output);
    }

    @Override
    public void onClick(View view) {
        String value = output.getText().toString();

        switch (view.getId()) {
            case R.id.btn0:
                //Toast.makeText(getApplicationContext(),"0", Toast.LENGTH_LONG).show();
                value = getOutputValue(value, "0");
                break;
            case R.id.btn1:
                value = getOutputValue(value, "1");
                break;
            case R.id.btn2:
                value = getOutputValue(value, "2");
                break;
            case R.id.btn3:
                value = getOutputValue(value, "3");
                break;
            case R.id.btn4:
                value = getOutputValue(value, "4");
                break;
            case R.id.btn5:
                value = getOutputValue(value, "5");
                break;
            case R.id.btn6:
                value = getOutputValue(value, "6");
                break;
            case R.id.btn7:
                value = getOutputValue(value, "7");
                break;
            case R.id.btn8:
                value = getOutputValue(value, "8");
                break;
            case R.id.btn9:
                value = getOutputValue(value, "9");
                break;
            case R.id.btn_dot:
                value = getOutputValueWithDot(value, ".");
                break;

            case R.id.btn_plus:
                //value = makeOperation(value, "+");
                SaveValue(value, "+");
                break;
            case R.id.btn_minus:
                //value = makeOperation(value, "-");
                SaveValue(value, "-");
                break;
            case R.id.btn_mult:
                //value = makeOperation(value, "*");
                SaveValue(value, "*");
                break;
            case R.id.btn_div:
                //value = makeOperation(value, "/");
                SaveValue(value, "/");
                break;
            case R.id.btn_sqrt:
                Double res = Math.sqrt(Double.parseDouble(value));
                value = res.toString();
                break;
            case R.id.btn_ans:
                value = this.current.toString();
                break;
            case R.id.btn_equal:
                if (!value.isEmpty())
                    value = makeOperation(this.lastValue, lastop);
                break;

            case R.id.btn_del:
                value = delOutputValue(value);
                break;
            case R.id.btn_ac:
                current = 0.0;
                lastValue = 0.0;
                lastop = "";
                value = "0";
                isNew = false;
                break;
        }
        output.setText(value);
    }

    private String getOutputValueWithDot(String value, String s) {
        if (!value.isEmpty() && !value.contains(".") ) {
            this.lastValue = Double.parseDouble(value+s);
            return value + s;
        } else {
            return value;
        }
    }

    public String delOutputValue(String str) {
        if (str != null && str.length() > 0) {
            str = str.substring(0, str.length() - 1);

        }

        if (str.isEmpty())
            return "0";

        return str;
    }

    private String getOutputValue(String value, String s) {

        if (!value.isEmpty() && !value.contentEquals("0") ) {
            if (isNew) {
                this.isNew = false;
                this.lastValue = Double.parseDouble(s);
                return s;
            }
            this.lastValue = Double.parseDouble(value+s);
            return value + s;
        }
        else {
            this.lastValue = Double.parseDouble(s);
            return s;
        }
    }

    private void SaveValue(String value, String op) {
        try {
            this.current = Double.parseDouble(value);
            this.lastop = op;
            //this.lastValue;
            this.isNew = true;
        } catch(Exception err){}
    }

    private String makeOperation(Double num, String s) {

        try {
            //Integer num = Integer.parseInt(value);

            switch (s) {
                case "+":
                    this.current += num;
                    break;
                case "-":
                    this.current -= num;
                    break;
                case "*":
                    this.current *= num;
                    break;
                case "/":
                    this.current = this.current / num;
                    break;
                default:
                    break;
            }

            this.lastop = s;
            this.lastValue = num;
        } catch(Exception err){}

        System.out.println(this.current);
        this.isNew = true;
        return this.current.toString();
    }


}